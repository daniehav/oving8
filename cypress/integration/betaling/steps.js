import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given('at jeg har lagt inn varer i handlekurven', () => {
    cy.visit('http://localhost:8080')
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();
})

And('trykket på Gå til betaling', () => {
    cy.get('#goToPayment').click()
})

When('jeg legger inn navn, adresse, postnummer, poststed og kortnummer', () => {
    cy.get('#fullName').clear().type('Daniel Havlas');
    cy.get('#address').clear().type('apeveien 1');
    cy.get('#postCode').clear().type('6421');
    cy.get('#city').clear().type('Molde');
    cy.get('#creditCardNo').clear().type('4172600043139425');
})

And('trykker på Fullfør kjøp', () => {
    cy.get('form').submit()
})

Then('skal jeg få beskjed om at kjøpet er registrert', () => {
    cy.get('.confirmation').should('exist')
})

When('jeg legger inn ugyldige verdier i feltene', () => {

    cy.get('#fullName').clear().blur();
    cy.get('#address').clear().blur();
    cy.get('#postCode').clear().blur();
    cy.get('#city').clear().blur();
    cy.get('#creditCardNo').type('123123123123').blur()
    
})

Then('skal jeg få feilmeldinger for disse', () => {
    cy.get('#fullNameError').should('have.text','Feltet må ha en verdi')
    cy.get('#addressError').should('have.text','Feltet må ha en verdi')
    cy.get('#cityError').should('have.text','Feltet må ha en verdi')
    cy.get('#postCodeError').should('have.text','Feltet må ha en verdi')
    cy.get('#creditCardNoError').should('have.text','Kredittkortnummeret må bestå av 16 siffer')
})