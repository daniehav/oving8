import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

When(/^jeg legger inn varer og kvanta$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

Then(/^skal handlekurven inneholde det jeg har lagt inn$/, () => {
    cy.get('#list').find('li').should('have.lengthOf',4)
    cy.contains('#list','4 Hubba bubba; 8 kr').should('exist')
    cy.contains('#list','5 Smørbukk; 5 kr').should('exist')
    cy.contains('#list','1 Stratos; 8 kr').should('exist')
    cy.contains('#list','2 Hobby; 12 kr').should('exist')
});

And(/^den skal ha riktig totalpris$/, () => {
    cy.get('#price').should('have.text', '33');
});

// Scenario 2

Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
})

And(/^lagt inn varer og kvanta$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();
})

When(/^jeg sletter varer$/, () => {
    cy.get('#emptyCart').click()
})

Then(/^skal ikke handlekurven inneholde det jeg har slettet$/, () => {
    cy.get('#list').find('li').should('have.lengthOf',0)
})

//scenario 3

When('jeg oppdaterer kvanta for en vare', () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
})

Then('skal handlekurven inneholde riktig kvanta for varen', () => {
    cy.get('#list>li').should('have.text','2 Hubba bubba; 4 kr')
})



